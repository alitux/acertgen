# Generador de Certificados para Facturación Electrónica con AFIP

Acertgen simplifica la generación de claves privadas y solicitudes de certificados SSL/TLS necesarios para la facturación electrónica con la Administración Federal de Ingresos Públicos (AFIP) en Argentina.

## Requisitos

- Python 3.x
- OpenSSL instalado en el sistema

## Uso

1. Clona o descarga este repositorio a tu máquina local.

2. Abre una terminal y navega al directorio donde se encuentra este programa.

3. Ejecuta el programa con los siguientes comandos:

   ```shell
   python acertgen.py --n nombre_llave.key --p nombre_pedido.cer --r "Razón Social" --h hostname.com --c CUIT
   ```
    - --n: Nombre deseado para la clave privada (opcional, predeterminado: llave_privada.key).
    - --p: Nombre deseado para la solicitud de certificado (opcional, predeterminado: pedido.cer).
    - --r: Razón social de la empresa.
    - --h: Nombre de host (hostname) para el certificado.
    - --c: Número de identificación fiscal (CUIT) de la empresa.

    El programa generará la clave privada y la solicitud de certificado con los nombres especificados y mostrará los nombres de los archivos generados.    --c: Número de identificación fiscal (CUIT) de la empresa.

## Ejemplo

    ```shell
    python acertgen.py --r "Mi Empresa" --h mihostname.com --c 1234567890
    ```

    Esto generará una clave privada con el nombre llave_privada.key y una solicitud de certificado con el nombre pedido.cer, utilizando la razón social, el nombre de host y el CUIT proporcionados.

## Notas

    Asegúrate de tener OpenSSL instalado en tu sistema para que el programa funcione correctamente.
    Los nombres de los archivos generados se pueden personalizar mediante los argumentos de línea de comandos.
    Este programa simplifica la generación de certificados, pero asegúrate de seguir todas las regulaciones y políticas de seguridad de AFIP al utilizar los certificados en aplicaciones de facturación electrónica.
