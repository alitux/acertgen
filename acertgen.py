#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import subprocess
import sys
from argparse import ArgumentParser

"""Acertgen
Acertgen se utiliza para generar claves privadas y solicitudes de certificados SSL/TLS 
necesarios para la facturación electrónica con AFIP (Administración Federal de Ingresos Públicos) en Argentina. 
Permite a los usuarios personalizar la generación de estos certificados mediante argumentos 
de línea de comandos, como la razón social, el nombre de host y el número de identificación fiscal (CUIT), 
y muestra los nombres de los archivos generados.
"""


def generate_key(nombre_llave):
    # Genera la llave privada
    llave = subprocess.run(
        f"openssl genrsa -out {nombre_llave} 2048", shell=True)
    return (llave.check_returncode())


def generate_request(nombre_llave, razon_social, hostname, cuit, nombre_pedido):
    #Genera la Solicitud de Certificado SSL/TLS
    pedido = subprocess.run(
        f"openssl req -new -key {nombre_llave} -subj '/C=AR/O={razon_social}/CN={hostname}/serialNumber=CUIT {cuit}' -out {nombre_pedido}", shell=True)
    return (pedido.check_returncode())


arg_parser = ArgumentParser()
arg_parser.add_argument('--n', help='Nombre de la Llave Privada')
arg_parser.add_argument('--p', help='Nombre del Pedido')
arg_parser.add_argument('--r', help='Razón social')
arg_parser.add_argument('--h', help='Server Hostname')
arg_parser.add_argument('--c', help='CUIT (Sin Guiones)')

args = arg_parser.parse_args()

if args.n == None:
    args.n = "llave_privada.key"

if args.p == None:
    args.p = "pedido.cer"

try:
    llave = generate_key(nombre_llave=args.n)
except:
    print("No se pudo generar la llave")
    sys.exit(1)

print(f"LLAVE PRIVADA: {args.n}")

try:
    pedido = generate_request(nombre_llave=args.n, razon_social=args.r,
                              hostname=args.h, cuit=args.c, nombre_pedido=args.p)
except:
    print("No se pudo generar el pedido")
    sys.exit(1)

print(f"PEDIDO: {args.p}")
